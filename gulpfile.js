'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var less = require('gulp-less');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var pug = require('gulp-pug');
var babel = require("gulp-babel");
var browserSync = require('browser-sync').create();
var uglify = require('gulp-uglify');
var pump = require('pump');
var autoprefixer = require('gulp-autoprefixer');


var config = {
	src_path : './src/',
	dest_path : './build/',
	bootstrap_path : './node_modules/bootstrap-sass/assets/',
	jquery_path : './node_modules/jquery/dist/'
}



gulp.task('setup-assets' , function(){
	
	
	//setup/copy bootstrap fonts to build directory
	
	gulp.src(config.bootstrap_path + 'fonts/bootstrap/**.*')
	.pipe(gulp.dest(config.dest_path + 'fonts/bootstrap'))
	
	//copy jquery js files to build directory
	
	gulp.src(config.jquery_path + 'jquery.min.js')
	.pipe(gulp.dest(config.dest_path + 'vendor/jquery'))
	
	//copy bootstrap js files to build directory
	
	gulp.src(config.bootstrap_path + 'javascripts/bootstrap.min.js')
	.pipe(gulp.dest(config.dest_path + 'vendor/bootstrap/js'))
	
});

gulp.task('sass' , function(){
	
	return gulp.src(config.src_path+'sass/**/*.scss')
		.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
		.pipe(sass({
			includePaths : [
				config.bootstrap_path+"stylesheets"
			]
		}))
		.pipe(autoprefixer())
		.pipe(gulp.dest(config.dest_path+'css'))
		.pipe(browserSync.stream());

});


gulp.task('less',function(){
	
	return gulp.src(config.src_path + 'less/**/*.less')
		.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
		.pipe(less())
		.pipe(autoprefixer())
		.pipe(gulp.dest(config.dest_path+'css'))
		.pipe(browserSync.stream());
});

gulp.task('pug',function buildHTML(){
	
	return gulp.src(config.src_path + 'views/**/!(_)*.pug')
		.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
		.pipe(pug({ 
			pretty : true
		}))
		.pipe(gulp.dest(config.dest_path))
		.pipe(browserSync.stream());
});


gulp.task('js',function(cb){
	
	pump([
		gulp.src(config.src_path + "js/**/*.js"),
		babel(),
		uglify(),
		gulp.dest(config.dest_path + "js"),
		browserSync.stream()
    ],
    cb
  );
});

gulp.task('serve',['sass','pug','js','setup-assets'],function(){
	
	browserSync.init({
        server: config.dest_path
    });

	gulp.watch(config.src_path + 'sass/**/*.scss',['sass']);
	//gulp.watch('./less/**/*.less',['less']);
	gulp.watch(config.src_path + 'views/**/*.pug',['pug']);
	gulp.watch(config.src_path + 'js/**/*.js',['js']);
	gulp.watch(config.src_path + 'views/**/(_)*.pug').on('change',browserSync.reload);
	//gulp.watch('./build/*.html').on('change',browserSync.reload);

});

gulp.task('default',['serve']);